namespace ClinicalAgenda.Data.Extensions
{
  public static class GenericExtensions
  {
    public static void Copy<T>(this T to, T from)
    {
      if (from != null)
      {
        foreach (var propertyFrom in from.GetType().GetProperties())
        {
          foreach (var propertyTo in to.GetType().GetProperties())
          {
            if (propertyTo.Name == propertyFrom.Name)
            {
              var value = propertyFrom.GetValue(from, null);
              propertyTo.SetValue(to, value, null);
            }
          }
        }
      }
    }

    public static void CopyTo<T>(this T from, T to) where T : class, new()
    {
      to.Copy(from);
    }

  }
}