﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicalAgenda.Domain;
using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Data.Extensions;

namespace ClinicalAgenda.Data.Repositories
{
  public class Repository<T> : IRepository<T> where T : IEntity
  {
    protected int currentId = 0;
    protected List<T> itens;

    public Repository()
    {
      this.itens = new List<T>();
    }

    public int Size()
    {
      return itens.Count;
    }

    public List<T> GetAll()
    {
      return itens;
    }

    public void Add(T t)
    {
      if (t.Id == 0)
      {
        currentId = currentId + 1;
        t.Id = currentId;
      }
      itens.Add(t);
    }

    public T Find(int Id)
    {
      return itens.Find(item => item.Id == Id);
    }

    public void Delete(T t)
    {
      T toRemove = Find(t.Id);
      itens.Remove(toRemove);
    }

    public void Delete(int id)
    {
      T toRemove = Find(id);
      itens.Remove(toRemove);
    }

    public void Update(T t)
    {
      Update(t.Id, t);
    }

    public void Update(int id, T t)
    {

      T saved = Find(id);
      saved.Copy(t);

    }

  }
}