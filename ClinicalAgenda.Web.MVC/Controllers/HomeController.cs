﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ClinicalAgenda.Web.MVC.Models;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Domain;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Web.MVC.Extensions;

namespace ClinicalAgenda.Web.MVC.Controllers
{
  public class HomeController : BaseController<IEntity>
  {
    public HomeController(IServiceFacade facade) : base(facade)
    {

    }

    public IActionResult Index()
    {

      var schedules = _facade.GetScheduleService().GetMySchedules(CurrentUser).Transform<Schedule, ScheduleViewModel>();

      return View(schedules);
    }

    public IActionResult About()
    {
      ViewData["Message"] = "Your application description page.";

      return View();
    }

    public IActionResult Contact()
    {
      ViewData["Message"] = "Your contact page.";

      return View();
    }

    public IActionResult Privacy()
    {
      return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}
