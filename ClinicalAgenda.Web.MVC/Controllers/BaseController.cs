using Microsoft.AspNetCore.Mvc;
using ClinicalAgenda.Domain;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Web.MVC.Extensions;
using System;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using ClinicalAgenda.Domain.Interfaces;

namespace ClinicalAgenda.Web.MVC.Controllers
{
  public abstract class BaseController<T> : Controller where T : IEntity
  {
    protected IServiceFacade _facade;

    public BaseController(IServiceFacade facade)
    {
      _facade = facade;
    }

    public bool SkipAuthentication { get; set; }

    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
      ViewBag.CurrentUser = CurrentUser;

      ViewBag.IsClient = false;
      if (CurrentUser != null)
      {
        ViewBag.IsClient = CurrentUser is Client;
      }

      if (!SkipAuthentication && ViewBag.CurrentUser == null)
      {
        filterContext.Result = new RedirectToRouteResult(
          new RouteValueDictionary
          {
              {"controller", "Sessions"},
              {"action", "Create"}
          }
        );
      }
    }

    public User CurrentUser
    {
      get
      {
        var id = HttpContext.Session.Get<String>("CurrentUserId").ToInt();

        return _facade.GetUserService().Find(id);
      }
      set
      {
        if (value != null)
        {
          HttpContext.Session.Set<String>("CurrentUserId", value.Id.ToString());
        }
      }
    }

    protected IActionResult RespondTo(IValidationResult<T> result, IActionResult successAction, IActionResult failAction)
    {
      if (result.IsValid() && ModelState.IsValid)
      {
        return successAction;
      }
      else
      {
        foreach (var error in result.Errors)
        {
          ModelState.AddModelError(error.GetField(), error.GetMessage());
        }

        return failAction;
      }
    }
  }
}