﻿
using ClinicalAgenda.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using ClinicalAgenda.Web.MVC.Models;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Web.MVC.Extensions;
using ClinicalAgenda.Web.MVC.Controllers;
using ClinicalAgenda.Domain.Interfaces;

namespace ClinicalAgenda.Web.Controllers
{
  public class ClientsController : BaseController<User>
  {

    public ClientsController(IServiceFacade facade) : base(facade)
    {

    }
    // GET: Clients/
    public IActionResult Index()
    {
      var clients = _facade.GetUserService().GetAllClients().Transform<Client, ClientViewModel>();

      return View(clients);
    }

    // GET: Clients/Details/5
    public IActionResult Details(int id)
    {
      var client = _facade.GetUserService().FindClient(id);
      var viewModel = client.Transform<Client, ClientViewModel>();
      LoadSchedulesFor(client);
      if (client is Client)
        return View(viewModel);
      else
      {
        return RedirectToAction("Index");
      }
    }

    // GET: Clients/Edit/5
    public IActionResult Edit(int id)
    {
      var client = _facade.GetUserService().FindClient(id);
      var viewModel = client.Transform<Client, ClientViewModel>();
      if (client is Client)
      {
        return View(viewModel);
      }
      else
      {
        return RedirectToAction("Index");
      }
    }

    // POST: Clients/Edit/5
    [HttpPost]
    public IActionResult Edit(int id, ClientViewModel viewModel)
    {
      var client = viewModel.Transform<ClientViewModel, Client>();
      var result = _facade.GetUserService().Update(client);

      return RespondTo(result, RedirectToAction("Details", new { id = client.Id }), Edit(id));
    }

    private void LoadSchedulesFor(Client client)
    {
      ViewBag.Schedules = _facade.GetScheduleService().SelectByClient(client).Transform<Schedule, ScheduleViewModel>();
    }

  }
}
