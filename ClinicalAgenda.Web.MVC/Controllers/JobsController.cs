﻿using ClinicalAgenda.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Web.MVC.Models;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Web.MVC.Extensions;
using ClinicalAgenda.Domain.Interfaces.Services;

using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Web.MVC.Controllers;
using ClinicalAgenda.Domain.Interfaces;
using System;

namespace ClinicalAgenda.Web.Controllers
{
  public class TreatmentsController : BaseController<Treatment>
  {
    public TreatmentsController(IServiceFacade facade) : base(facade)
    {

    }
    // GET: Schedules
    public IActionResult Index()
    {
      var treatments = _facade.GetTreatmentService().GetAll().Transform<Treatment, TreatmentViewModel>();

      return View(treatments);
    }

    // GET: Schedules/Details/5
    public IActionResult Details(int id)
    {
      var schedule = _facade.GetTreatmentService().Find(id).Transform<Treatment, TreatmentViewModel>();
      return View(schedule);
    }

    // GET: Schedules/Create
    public IActionResult Create()
    {
      LoadTreatmentTypes();
      return View();
    }

    // POST: Schedules/Create
    [HttpPost]
    public IActionResult Create(TreatmentViewModel viewModel)
    {
      var Treatment = viewModel.Transform<TreatmentViewModel, Treatment>();

      var result = _facade.GetTreatmentService().Add(Treatment);
      return RespondTo(result, RedirectToAction("Index"), Create());
    }

    // GET: Schedules/Edit/5
    public IActionResult Edit(int id)
    {
      LoadTreatmentTypes();

      var Treatment = _facade.GetTreatmentService().Find(id).Transform<Treatment, TreatmentViewModel>();
      return View(Treatment);
    }

    // POST: Schedules/Edit/5
    [HttpPost]
    public IActionResult Edit(int id, TreatmentViewModel viewModel)
    {

      var Treatment = viewModel.Transform<TreatmentViewModel, Treatment>();
      var result = _facade.GetTreatmentService().Update(Treatment);

      return RespondTo(result, RedirectToAction("Index"), Edit(id));
    }

    // GET: Schedules/Delete/5
    public IActionResult Delete(int id)
    {
      var schedule = _facade.GetTreatmentService().Find(id).Transform<Treatment, TreatmentViewModel>();
      return View(schedule);

    }

    // POST: /Schdules/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public IActionResult DeleteConfirmed(int id)
    {
      var result = _facade.GetTreatmentService().Remove(id);

      return RespondTo(result, RedirectToAction("Index"), Delete(id));
    }

    private void LoadTreatmentTypes()
    {
      TreatmentType[] treatmentsTypes = (TreatmentType[])Enum.GetValues(typeof(TreatmentType));
      ViewBag.TreatmentTypes = treatmentsTypes.Select(c =>
        new SelectListItem()
        {
          Text = c.ToString(),
          Value = c.ToString()
        });
    }

  }
}
