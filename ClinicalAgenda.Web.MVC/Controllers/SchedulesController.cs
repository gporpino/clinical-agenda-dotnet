﻿using ClinicalAgenda.Data.Repositories;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Web.MVC.Models;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Web.MVC.Extensions;
using ClinicalAgenda.Domain.Interfaces.Services;

using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Web.MVC.Controllers;
using ClinicalAgenda.Domain.Interfaces;
using System;

namespace ClinicalAgenda.Web.Controllers
{
  public class SchedulesController : BaseController<Schedule>
  {
    public SchedulesController(IServiceFacade facade) : base(facade)
    {

    }
    // GET: Schedules
    public IActionResult Index()
    {
      var schedules = _facade.GetScheduleService().GetMySchedules(CurrentUser).Transform<Schedule, ScheduleViewModel>();

      return View(schedules);
    }

    // GET: Schedules/Details/5
    public IActionResult Details(int id)
    {
      var schedule = _facade.GetScheduleService().Find(id).Transform<Schedule, ScheduleViewModel>();
      return View(schedule);
    }

    // GET: Schedules/Create
    public IActionResult Create()
    {
      if (CurrentUser is Client)
      {
        BeforeCreateOrEdit();
        return View();
      }
      else
      {
        return RedirectToAction("Index");
      }
    }

    // POST: Schedules/Create
    [HttpPost]
    public IActionResult Create(ScheduleViewModel viewModel)
    {
      var schedule = viewModel.Transform<ScheduleViewModel, Schedule>((vm, s) =>
      {
        s.Client = _facade.GetUserService().FindClient(CurrentUser.Id);
        s.Treatment = _facade.GetTreatmentService().Find(vm.TreatmentId);
      });

      var result = _facade.GetScheduleService().Add(schedule);
      return RespondTo(result, RedirectToAction("Index"), Create());
    }

    // GET: Schedules/Edit/5
    public IActionResult Edit(int id)
    {
      BeforeCreateOrEdit();

      var schedule = _facade.GetScheduleService().Find(id).Transform<Schedule, ScheduleViewModel>();
      if (schedule.Client.Id == CurrentUser.Id)
      {
        return View(schedule);
      }
      else
      {
        return RedirectToAction("Index");
      }
    }

    // POST: Schedules/Edit/5
    [HttpPost]
    public IActionResult Edit(int id, ScheduleViewModel viewModel)
    {

      var schedule = viewModel.Transform<ScheduleViewModel, Schedule>((vm, s) =>
      {
        s.Client = _facade.GetUserService().FindClient(vm.ClientId);
        s.Treatment = _facade.GetTreatmentService().Find(vm.TreatmentId);
      });
      var result = _facade.GetScheduleService().Update(schedule);

      return RespondTo(result, RedirectToAction("Index"), Edit(id));
    }

    // GET: Schedules/Delete/5
    public IActionResult Delete(int id)
    {
      var schedule = _facade.GetScheduleService().Find(id).Transform<Schedule, ScheduleViewModel>();
      return View(schedule);

    }

    // POST: /Schdules/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public IActionResult DeleteConfirmed(int id)
    {
      var result = _facade.GetScheduleService().Remove(id);

      return RespondTo(result, RedirectToAction("Index"), Delete(id));
    }

    //private methods

    private void BeforeCreateOrEdit()
    {
      LoadTreatments();
      LoadStartTimes();
      LoadMySchedules();
      LoadSessionDurations();
    }
    private void LoadSessionDurations()
    {
      var items = new List<SelectListItem>()
      {
        new SelectListItem { Text = "Choose one...", Value="" },
        new SelectListItem { Text = "30 minutes", Value="00:30:00" },
        new SelectListItem { Text = "1 hour", Value="1:00:00" }
      };
      ViewBag.SessionDurations = items;
    }

    private void LoadTreatments()
    {
      var treatments = _facade.GetTreatmentService().GetAll().Select(c =>
        new SelectListItem()
        {
          Text = c.Name,
          Value = c.Id.ToString()
        });
      treatments = treatments.Prepend(new SelectListItem()
      {
        Text = "Choose one...",
        Value = ""
      });
      ViewBag.Treatments = treatments;
    }

    private void LoadMySchedules()
    {
      List<Schedule> items = _facade.GetScheduleService().GetMySchedules(CurrentUser);
      ViewBag.MySchedules = items.Transform<Schedule, ScheduleViewModel>();
    }

    private void LoadStartTimes()
    {

      var items = new List<SelectListItem>()
      {
        new SelectListItem { Text = "Choose one...", Value="" },
        new SelectListItem { Text = "6:00", Value="06:00:00" },
        new SelectListItem { Text = "6:30", Value="06:30:00" },
        new SelectListItem { Text = "7:00", Value="07:00:00" },
        new SelectListItem { Text = "7:30", Value="07:30:00" },
        new SelectListItem { Text = "8:00", Value="08:00:00" },
        new SelectListItem { Text = "8:30", Value="08:30:00" },
        new SelectListItem { Text = "9:00", Value="09:00:00" },
        new SelectListItem { Text = "9:30", Value="09:30:00" },
        new SelectListItem { Text = "10:00", Value="10:00:00" },
        new SelectListItem { Text = "10:30", Value="10:30:00" },
        new SelectListItem { Text = "11:00", Value="11:00:00" },
        new SelectListItem { Text = "11:30", Value="11:30:00" },
        new SelectListItem { Text = "12:00", Value="12:00:00" },
        new SelectListItem { Text = "12:30", Value="12:30:00" },
        new SelectListItem { Text = "13:00", Value="13:00:00" },
        new SelectListItem { Text = "13:30", Value="13:30:00" },
        new SelectListItem { Text = "14:00", Value="14:00:00" },
        new SelectListItem { Text = "14:30", Value="14:30:00" },
        new SelectListItem { Text = "15:00", Value="15:00:00" },
        new SelectListItem { Text = "15:30", Value="15:30:00" },
        new SelectListItem { Text = "16:00", Value="16:00:00" },
        new SelectListItem { Text = "16:30", Value="16:30:00" },
        new SelectListItem { Text = "17:00", Value="17:00:00" },
        new SelectListItem { Text = "17:30", Value="17:30:00" },
        new SelectListItem { Text = "18:00", Value="18:00:00" },
        new SelectListItem { Text = "18:30", Value="18:30:00" },
        new SelectListItem { Text = "19:00", Value="19:00:00" },
        new SelectListItem { Text = "19:30", Value="19:30:00" },
        new SelectListItem { Text = "20:00", Value="20:00:00" },
        new SelectListItem { Text = "20:30", Value="20:30:00" },
        new SelectListItem { Text = "21:00", Value="21:00:00" },
        new SelectListItem { Text = "21:30", Value="21:30:00" },
        new SelectListItem { Text = "22:00", Value="22:00:00" },
        new SelectListItem { Text = "22:30", Value="22:30:00" },



      };
      ViewBag.StartTimes = items;
    }

  }
}
