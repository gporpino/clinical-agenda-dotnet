﻿
using System.Collections.Generic;
using System.Linq;
using ClinicalAgenda.Web.MVC.Models;
using ClinicalAgenda.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Web.MVC.Extensions;

namespace ClinicalAgenda.Web.MVC.Controllers
{
  public class SessionsController : BaseController<User>
  {

    public SessionsController(IServiceFacade facade) : base(facade)
    {
      SkipAuthentication = true;
      _facade = facade;
    }

    // GET: Sessions/Create
    public IActionResult Create()
    {

      return View();
    }

    // POST: Sessions/Create
    [HttpPost]
    public IActionResult Create(ClientViewModel viewModel)
    {

      var client = _facade.GetUserService().Authenticate(viewModel.Email, viewModel.Password);
      if (client != null)
      {
        CurrentUser = client;
        return RedirectToAction("Index", "Home");
      }
      else
      {
        ModelState.AddModelError(string.Empty, "Your user or password was wrong. Please check and try again.");

        return View();
      }
    }

    // GET: Sessions/Destroy
    public IActionResult Destroy()
    {
      CurrentUser = null;
      return RedirectToAction("Create", "Sessions");
    }

    public IActionResult Register()
    {
      return View();
    }

    // POST: Clients/Register
    [HttpPost]
    public IActionResult Register(ClientViewModel viewModel)
    {
      var client = viewModel.Transform<ClientViewModel, Client>();

      var result = _facade.GetUserService().Add(client);

      return RespondTo(result, RedirectToAction("Create", "Sessions"), Register());
    }

  }
}
