using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicalAgenda.Web.MVC.Validations
{
  public class RestricPastDate : ValidationAttribute
  {
    public override bool IsValid(object date)
    {
      DateTime datetime = (DateTime)date;
      return datetime >= DateTime.Now;
    }

  }
}