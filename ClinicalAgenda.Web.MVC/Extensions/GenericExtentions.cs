﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ClinicalAgenda.Domain;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ClinicalAgenda.Web.MVC.Extensions
{
  public static class GenericExtentions
  {
    public static To Transform<From, To>(this From from) where To : class, new() where From : class, new()
    {
      return from.Transform<From, To>(null);
    }

    public static To Transform<From, To>(this From from, Action<From, To> action) where To : class, new() where From : class, new()
    {
      To toObject = null;
      if (from != null)
      {
        toObject = new To();
        foreach (var propertyFrom in from.GetType().GetProperties())
        {
          foreach (var propertyTo in toObject.GetType().GetProperties())
          {
            if (propertyTo.Name == propertyFrom.Name)
            {
              var value = propertyFrom.GetValue(from, null);
              propertyTo.SetValue(toObject, value, null);
            }
            else if (propertyTo.Name == propertyFrom.Name + "Id")
            {
              var value = propertyFrom.GetValue(from, null);
              propertyTo.SetValue(toObject, value.Try("Id"), null);
            }
          }
        }
        if (action != null)
        {
          action.Invoke(from, toObject);
        }
      }

      return toObject;
    }

    public static IEnumerable<To> Transform<From, To>(this IEnumerable<From> list) where To : class, new() where From : class, new()
    {
      return list.Transform<From, To>(null);
    }

    public static IEnumerable<To> Transform<From, To>(this IEnumerable<From> list, Action<From, To> action) where To : class, new() where From : class, new()
    {
      IList<To> transformedList = null;
      if (list != null)
      {
        transformedList = new List<To>();
        foreach (var from in list)
        {
          var transformed = from.Transform<From, To>(action);
          transformedList.Add(transformed);
        }
      }

      return transformedList;
    }

    public static object Try<T>(this T t, Func<T, object> func)
    {
      try
      {
        return func.Invoke(t);
      }
      catch (Exception)
      {
        return null;
      }
    }

    public static object Try<T>(this T t, String propertyName)
    {
      try
      {
        var prop = t.GetType().GetProperty(propertyName);
        return prop.GetValue(t);
      }
      catch (Exception)
      {
        return null;
      }
    }

    public static void Set<T>(this ISession session, string key, T value)
    {
      session.SetString(key, JsonConvert.SerializeObject(value));
    }

    public static T Get<T>(this ISession session, string key)
    {
      var value = session.GetString(key);

      return value == null ? default(T) :
          JsonConvert.DeserializeObject<T>(value);
    }

    public static bool Is<K>(this Type type)
    {
      return type == typeof(K);
    }

    public static int ToInt(this String str)
    {
      try
      {
        return System.Convert.ToInt32(str);
      }
      catch
      {
        return 0;
      }
    }
  }
}
