

using System;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;

namespace ClinicalAgenda.Web.MVC.Extensions
{
  public static class HtmlHelperExtensions
  {

    public static IHtmlContent DescriptionFor<TModel, TValue>(this IHtmlHelper<TModel> self,
            Expression<Func<TModel, TValue>> expression)
    {
      var modelExplorer = ExpressionMetadataProvider.FromLambdaExpression(expression, self.ViewData, self.MetadataProvider);
      var metadata = modelExplorer.Metadata;

      if (metadata.Description != null)
      {
        var tag = "<span tabindex='0' class='badge badge-light' data-toggle='popover' data-trigger='focus' title=\"{0}\" data-content=\"{1}\">?</span>";
        return new HtmlString(string.Format(tag, metadata.DisplayName, metadata.Description));
      }
      else
      {
        return null;
      }

    }


  }
}