﻿using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Domain.Implementations;
using ClinicalAgenda.Domain.Implementations.Services;
using ClinicalAgenda.Domain.Interfaces;

namespace ClinicalAgenda.Web.MVC.Extensions
{
  public static class BindingExtensions
  {
    public static void LoadBindings(this IServiceCollection services)
    {
      //repositories
      services.AddSingleton<IRepository<Schedule>, Repository<Schedule>>();
      services.AddSingleton<IRepository<User>, Repository<User>>();
      services.AddSingleton<IRepository<Treatment>, Repository<Treatment>>();

      //services
      services.AddSingleton<IScheduleService, ScheduleService>();
      services.AddSingleton<ITreatmentService, TreatmentService>();
      services.AddSingleton<IUserService, UserService>();


      //Facade
      services.AddSingleton<IServiceFacade, ServiceFacade>();

      //services.AddScoped<IRenderScoped, RenderScoped>();

      //services.AddTransient<IRenderTransient, RenderTransient>();
    }
  }
}
