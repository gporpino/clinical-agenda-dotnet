﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicalAgenda.Domain;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Web.MVC.Models
{
  public class ClientViewModel : IEntity
  {
    public int Id { get; set; }

    [Required(ErrorMessage = "{0} cant't be blank")]

    [Display(Name = "Name", Description = "Fill with your name. It can't be blank.")]
    public String Name { get; set; }

    [Display(Name = "Surname", Description = "Fill with your surname. It is not required.")]
    public String Surname { get; set; }

    [Required(ErrorMessage = "{0} is required")]
    [Display(Name = "Document", Description = "Fill with your document. It can be CNH or CPF. It can't be blank.")]
    public String Document { get; set; }

    [Required(ErrorMessage = "{0} is required")]
    [Display(Name = "Cellphone", Description = "Fill with your cellphone. It can't be blank.")]
    public String Cellphone { get; set; }

    [Display(Name = "Address", Description = "Fill with your full address. Don't forget the number and nightborhood. It is not required.")]
    public String Address { get; set; }

    [Display(Name = "E-mail", Description = "Fill with your e-mail.")]
    public String Email { get; set; }

    [DataType(DataType.Password)]
    [Display(Name = "Password", Description = "Fill with your password.")]
    public String Password { get; set; }



  }
}
