﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicalAgenda.Domain;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Web.MVC.Models
{
  public class TreatmentViewModel : IEntity
  {
    public int Id { get; set; }

    [Required]
    [Display(Name = "Name", Description = "The name of Treatment. It can't be blank.")]
    public String Name { get; set; }

    [Required]
    [Display(Name = "Type", Description = "The type of Treatment. It can't be blank.")]
    public TreatmentType Type { get; set; }
  }
}
