﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicalAgenda.Domain;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;
using ClinicalAgenda.Web.MVC.Validations;

namespace ClinicalAgenda.Web.MVC.Models
{
  public class ScheduleViewModel : IEntity
  {
    public int Id { get; set; }

    [DataType(DataType.Date)]
    [Required(ErrorMessage = "{0} can't be blank")]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
    [RestricPastDate(ErrorMessage = "{0} must be on the future.")]
    [Display(Name = "Start Date", Description = "The start date of the schedule. It must be in the future.")]
    public DateTime StartDate { get; set; }

    [Required(ErrorMessage = "{0} can't be blank")]
    [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = true)]
    [Display(Name = "Start Time", Description = "The start time of the schedule. It must be from 8h.")]
    public TimeSpan StartTime { get; set; }

    [Required(ErrorMessage = "{0} can't be blank")]
    [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = true)]
    [Display(Name = "Session Duration", Description = "The start time of the schedule. The end time must be up to 16h.")]
    public TimeSpan SessionDuration { get; set; }

    public int ClientId { get; set; }

    [Required(ErrorMessage = "{0} can't be blank")]
    [Display(Name = "Treatment", Description = "The Treatment of the schedule. It can`t be blank.")]

    public int TreatmentId { get; set; }

    public Client Client { get; set; }

    public Treatment Treatment { get; set; }
  }
}
