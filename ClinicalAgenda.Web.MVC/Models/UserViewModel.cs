﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicalAgenda.Domain;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Web.MVC.Models
{
  public class UserViewModel : IEntity
  {
    public int Id { get; set; }

    [Required(ErrorMessage = "{0} is required")]
    public String Name { get; set; }
    public String Surname { get; set; }
    public String Email { get; set; }

    [DataType(DataType.Password)]
    public String Password { get; set; }



  }
}
