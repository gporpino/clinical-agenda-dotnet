﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClinicalAgenda.Domain.Implementations.Validations.Validators;
using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Services
{
  public class ScheduleService : ServiceBase<Schedule>, IScheduleService
  {
    public ScheduleService(IRepository<Schedule> repository) : base(repository)
    {

    }

    public List<Schedule> SelectByClient(Client client)
    {
      return _repository.GetAll().Where(s => s.Client.Id == client.Id).ToList();
    }

    public List<Schedule> GetMySchedules(User user)
    {
      if (user is Client)
      {
        return _repository.GetAll().Where(s => s.Client.Id == user.Id).ToList();
      }
      else
      {
        return _repository.GetAll();
      }
    }

    public override void SetValidators(IValidationList<Schedule> validations)
    {
      validations.Add(new ScheduleStartTimeValidator(), ValidationType.CREATE, ValidationType.UPDATE);
      validations.Add(new ScheduleEndTimeValidator(), ValidationType.CREATE, ValidationType.UPDATE);
      validations.Add(new ScheduleTotalSessionDurationValidator(), ValidationType.CREATE, ValidationType.UPDATE);
      validations.Add(new ScheduleConflitTimeValidator(), ValidationType.CREATE, ValidationType.UPDATE);

      validations.Add(new ScheduleDeleteOrUpdateValidator(), ValidationType.DELETE, ValidationType.UPDATE);

    }
  }
}