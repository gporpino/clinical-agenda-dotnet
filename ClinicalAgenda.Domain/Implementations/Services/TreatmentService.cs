﻿using System;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Services
{
  public class TreatmentService : ServiceBase<Treatment>, ITreatmentService
  {
    public TreatmentService(IRepository<Treatment> repository) : base(repository)
    {

    }

    public override void SetValidators(IValidationList<Treatment> validators)
    {

    }
  }
}