﻿using System;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Implementations.Validations;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;


namespace ClinicalAgenda.Domain.Implementations.Services
{
  public abstract class ServiceBase<T> : IService<T> where T : IEntity
  {
    protected readonly IRepository<T> _repository;

    readonly ValidationList<T> _listValidation;

    public ServiceBase(IRepository<T> repository)
    {
      _repository = repository;
      _listValidation = new ValidationList<T>();
      SetValidators(_listValidation);
    }

    public virtual IValidationResult<T> Add(T t)
    {
      var result = Validate(t, ValidationType.CREATE);
      if (result.IsValid())
      {
        _repository.Add(t);
      }

      return result;
    }

    public T Find(int id)
    {
      return _repository.Find(id);
    }

    public List<T> GetAll()
    {
      return _repository.GetAll();
    }

    public IValidationResult<T> Remove(T t)
    {
      var result = Validate(t, ValidationType.DELETE);
      if (result.IsValid())
      {
        _repository.Delete(t);
      }
      return result;
    }

    public IValidationResult<T> Remove(int id)
    {
      T entity = Find(id);
      return Remove(entity);
    }

    public IValidationResult<T> Update(T t)
    {
      var result = Validate(t, ValidationType.UPDATE);
      if (result.IsValid())
      {
        _repository.Update(t);
      }

      return result;
    }

    public abstract void SetValidators(IValidationList<T> validations);


    public IValidationResult<T> Validate(T entity, ValidationType type)
    {

      var result = new ValidationResult<T>(entity);
      var context = new ValidationContext<T>() { Service = this, Entity = entity };

      foreach (var validation in _listValidation.GetValidations())
      {
        if (validation.Type == type && !validation.Validator.Validate(context))
        {
          result.AddError(validation, entity);
        }
      }
      return result;
    }

  }
}