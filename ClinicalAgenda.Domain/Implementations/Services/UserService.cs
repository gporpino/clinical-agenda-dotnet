﻿using System;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Interfaces.Repositories;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;
using System.Linq;
using ClinicalAgenda.Domain.Implementations.Validations.Validators;
using ClinicalAgenda.Domain.Interfaces;

namespace ClinicalAgenda.Domain.Implementations.Services
{
  public class UserService : ServiceBase<User>, IUserService
  {
    public UserService(IRepository<User> repository) : base(repository)
    {

    }

    public User Authenticate(string email, string password)
    {
      User client = _repository.GetAll().Where(c => c.Email == email).FirstOrDefault();

      if (client != null && client.Password == password)
      {
        return client;
      }
      return null;
    }

    public Client FindClient(int id)
    {
      return GetAllClients().Where(c => c.Id == id).FirstOrDefault();
    }

    public IList<Client> GetAllClients()
    {
      return _repository.GetAll().Where(c => c.GetType() == typeof(Client)).Cast<Client>().ToList();
    }

    public override void SetValidators(IValidationList<User> validations)
    {
      validations.Add(new UserDuplicatedEmailValidator(), ValidationType.CREATE, ValidationType.UPDATE);
    }
  }
}