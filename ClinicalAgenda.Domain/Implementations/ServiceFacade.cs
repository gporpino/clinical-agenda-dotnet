using System;
using System.Linq;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Domain.Implementations
{
  public class ServiceFacade : IServiceFacade
  {
    private IScheduleService _scheduleService;

    private ITreatmentService _treatmentService;
    private IUserService _userService;

    public ServiceFacade(IScheduleService scheduleService,
    ITreatmentService treatmentService, IUserService userService)
    {
      _scheduleService = scheduleService;
      _userService = userService;
      _treatmentService = treatmentService;

      //only for mock proporse
      MockServices();
    }

    public ITreatmentService GetTreatmentService()
    {
      return _treatmentService;
    }

    public IScheduleService GetScheduleService()
    {
      return _scheduleService;
    }

    public IUserService GetUserService()
    {
      return _userService;
    }

    //private methods
    private void MockServices()
    {
      //treatments
      _treatmentService.Add(new Treatment()
      {
        Name = "Linfatic Massage",
        Type = TreatmentType.Massage
      });

      _treatmentService.Add(new Treatment()
      {
        Name = "Podal Treatment",
        Type = TreatmentType.Treatment
      });

      //clients
      _userService.Add(new User()
      {
        Name = "John",
        Surname = "Scoot",
        Email = "test@user.com",
        Password = "123456",
      });

      _userService.Add(new Client()
      {
        Name = "Paul",
        Document = "090.087.567-11",
        Cellphone = "(81) 99898-9898",
        Surname = "McDonalds",
        Email = "test@client.com",
        Password = "123456",
        Address = "Rua A"
      });

      _userService.Add(new Client()
      {
        Name = "Mary",
        Surname = "Vivian",
        Document = "090.087.567-12",
        Cellphone = "(81) 99898-9897",
        Email = "test2@client.com",
        Password = "123456",
        Address = "Rua B"
      });

      _userService.Add(new Client()
      {
        Name = "Carla",
        Surname = "Krostoven",
        Document = "090.087.567-13",
        Cellphone = "(81) 99898-9896",
        Email = "test3@client.com",
        Password = "123456",
        Address = "Rua C"
      });

      var dateBase = DateTime.Today.Date.AddHours(12);
      //schedules
      _scheduleService.Add(new Schedule()
      {
        StartDate = dateBase.Date,
        StartTime = dateBase.TimeOfDay,
        SessionDuration = TimeSpan.Parse("0:30:00"),
        Client = _userService.GetAllClients().First(),
        Treatment = _treatmentService.GetAll().First()
      });

      _scheduleService.Add(new Schedule()
      {
        StartDate = dateBase.AddHours(24).Date,
        StartTime = dateBase.AddHours(24).TimeOfDay,
        SessionDuration = TimeSpan.Parse("0:30:00"),
        Client = _userService.GetAllClients().First(),
        Treatment = _treatmentService.GetAll().Last()
      });

      _scheduleService.Add(new Schedule()
      {
        StartDate = dateBase.AddDays(1).Date,
        StartTime = dateBase.AddDays(1).TimeOfDay,
        SessionDuration = TimeSpan.Parse("0:30:00"),
        Client = _userService.GetAllClients().Last(),
        Treatment = _treatmentService.GetAll().First()
      });
      _scheduleService.Add(new Schedule()
      {
        StartDate = dateBase.AddHours(48).Date,
        StartTime = dateBase.AddHours(48).TimeOfDay,
        SessionDuration = TimeSpan.Parse("0:30:00"),
        Client = _userService.GetAllClients().Last(),
        Treatment = _treatmentService.GetAll().Last()
      });
    }
  }
}