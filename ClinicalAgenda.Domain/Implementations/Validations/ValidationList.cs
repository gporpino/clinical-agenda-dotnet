using System;
using System.Linq;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain.Implementations.Validations
{
  public class ValidationList<T> : IValidationList<T> where T : IEntity
  {
    private List<IValidation<T>> _validations;


    public ValidationList()
    {
      _validations = new List<IValidation<T>>();
    }

    public void Add(IValidator<T> validator, params ValidationType[] typeParams)
    {
      ValidationType[] types = typeParams != null ? typeParams : (ValidationType[])Enum.GetValues(typeof(ValidationType));
      types.ToList().ForEach(t =>
      {
        _validations.Add(new Validation<T>(validator, t));
      });
    }

    public IList<IValidation<T>> GetValidations()
    {
      return _validations;
    }
  }
}