using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain.Implementations.Validations
{
  public class ValidationContext<T> : IValidationContext<T> where T : IEntity
  {
    public IService<T> Service { get; set; }
    public T Entity { get; set; }

  }
}