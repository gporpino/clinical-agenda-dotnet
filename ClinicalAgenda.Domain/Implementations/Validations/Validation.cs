using System.Collections.Generic;
using System.Linq;
using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain.Implementations.Validations
{
  public class Validation<T> : IValidation<T> where T : IEntity
  {

    internal Validation(IValidator<T> validator, ValidationType type)
    {
      this.Validator = validator;
      this.Type = type;
    }

    public IValidator<T> Validator { get; }

    public ValidationType Type { get; }
  }
}