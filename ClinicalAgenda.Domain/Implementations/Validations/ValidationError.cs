using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain.Implementations.Validations
{
  internal class ValidationError<T> : IValidationError<T> where T : IEntity
  {
    public IValidation<T> Validation { get; internal set; }

    public T Entity { get; internal set; }


    public string GetField()
    {
      return Validation.Validator.Field;
    }

    public string GetMessage()
    {
      return Validation.Validator.Message;
    }
  }
}