using System;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Validations.Validators
{
  public class ScheduleDeleteOrUpdateValidator : IValidator<Schedule>
  {
    public string Message => "You can't delete or edit schedule out of cancelation period.";

    public string Field => ""; //Base validation must set empty 

    public bool Validate(IValidationContext<Schedule> context)
    {
      var savedEntity = context.Service.Find(context.Entity.Id);
      if (savedEntity.Treatment.Type == TreatmentType.Massage)
      {
        var scheduleDate = savedEntity.StartDate + savedEntity.StartTime;
        var limitDateTime = (DateTime.Now.AddHours(24));
        if (limitDateTime < scheduleDate)
        {
          return true;
        }
      }
      else
      {
        if (DateTime.Now.Date < savedEntity.StartDate)
        {
          return true;
        }
      }
      return false;
    }
  }

}