using System;
using System.Linq;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Validations.Validators
{
  public class UserDuplicatedEmailValidator : IValidator<User>
  {
    public string Message => "A client with this email already exists.";

    public string Field => "Email"; //Base validation must set empty 

    public bool Validate(IValidationContext<User> context)
    {
      if (context.Entity.Email == null)
      {
        return true;
      }
      var users = context.Service.GetAll().Where(
        c => c.Email == context.Entity.Email && c.Id != context.Entity.Id);
      return users.Count() == 0;
    }
  }

}