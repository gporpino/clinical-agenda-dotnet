using System;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Validations.Validators
{
  public class ScheduleConflitTimeValidator : IValidator<Schedule>
  {
    public string Message => "You already have schedule for this period or it conflicts with the time of another interval.";

    public string Field => "";//Base validation must set empty 

    public bool Validate(IValidationContext<Schedule> context)
    {

      var startTimeEntity = context.Entity.StartTime;
      var endTimeEntity = context.Entity.StartTime + context.Entity.SessionDuration;

      if (context.Entity.SessionDuration != null && context.Entity.StartTime != null)
      {
        foreach (var s in context.Service.GetAll())
        {
          if (s.Id != context.Entity.Id && s.Client.Id == context.Entity.Client.Id)
          {
            var startTime = s.StartTime;
            var endTime = s.StartTime + s.SessionDuration;

            if (endTime > startTimeEntity && startTime < endTimeEntity)
            {
              return false;
            }
          }
        }
      }

      return true;
    }
  }

}