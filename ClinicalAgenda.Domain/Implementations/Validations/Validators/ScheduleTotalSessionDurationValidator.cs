using System;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Validations.Validators
{
  public class ScheduleTotalSessionDurationValidator : IValidator<Schedule>
  {
    public string Message => "You can't create more than 1 hour session on total.";

    public string Field => ""; //Base validation must set empty 

    public bool Validate(IValidationContext<Schedule> context)
    {
      var valid = context.Entity.SessionDuration != null && context.Entity.StartTime != null;

      var totalDuration = new TimeSpan();

      foreach (var s in context.Service.GetAll())
      {
        if (s.Id != context.Entity.Id && s.Client.Id == context.Entity.Client.Id)
        {
          totalDuration = totalDuration + s.SessionDuration;
        }
      }

      totalDuration = totalDuration + context.Entity.SessionDuration;

      if (totalDuration > TimeSpan.Parse("01:00:00"))
      {
        valid = false;
      }

      return valid;
    }
  }

}