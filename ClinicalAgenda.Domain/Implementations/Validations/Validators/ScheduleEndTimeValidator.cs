using System;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Validations.Validators
{
  public class ScheduleEndTimeValidator : IValidator<Schedule>
  {
    public string Message => "You must create an schedule up to 16hs.";

    public string Field => "SessionDuration";

    public bool Validate(IValidationContext<Schedule> context)
    {
      var valid = context.Entity.SessionDuration != null && context.Entity.StartTime != null;

      var startTime = context.Entity.StartTime;
      var finalTime = context.Entity.StartTime + context.Entity.SessionDuration;

      if (finalTime >= TimeSpan.Parse("16:00:00"))
      {
        valid = false;
      }

      return valid;
    }
  }

}