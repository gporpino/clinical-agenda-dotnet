using System;
using ClinicalAgenda.Domain.Interfaces;
using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Interfaces.Validations;
using ClinicalAgenda.Domain.Models;


namespace ClinicalAgenda.Domain.Implementations.Validations.Validators
{
  public class ScheduleStartTimeValidator : IValidator<Schedule>
  {
    public string Message => "You must create an schedule from 8h.";

    public string Field => "StartTime";

    public bool Validate(IValidationContext<Schedule> context)
    {
      var valid = context.Entity.SessionDuration != null && context.Entity.StartTime != null;

      var startTime = context.Entity.StartTime;

      if (startTime < TimeSpan.Parse("8:00:00"))
      {
        valid = false;
      }

      return valid;
    }
  }

}