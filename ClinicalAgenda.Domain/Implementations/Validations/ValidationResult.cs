using System;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain.Implementations.Validations
{
  internal class ValidationResult<T> : IValidationResult<T> where T : IEntity
  {
    public ValidationResult(IEntity entity)
    {
      Entity = entity;
      Errors = new List<IValidationError<T>>();
    }

    public bool IsValid()
    {
      return Errors.Count == 0;
    }

    public IList<IValidationError<T>> Errors { get; }

    public IEntity Entity { get; }

    public void AddError(IValidation<T> validation, T t)
    {
      Errors.Add(new ValidationError<T>()
      {
        Validation = validation,
        Entity = t
      });
    }


  }
}