﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalAgenda.Domain.Models
{
  public class Treatment : Entity
  {
    public String Name { get; set; }
    public TreatmentType Type { get; set; }
  }
}
