using System.Collections.Generic;
using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain.Models
{
  public class Entity : IEntity
  {
    public int Id { get; set; }

  }
}