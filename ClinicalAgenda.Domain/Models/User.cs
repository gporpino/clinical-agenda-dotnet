using System;
using ClinicalAgenda.Domain.Interfaces;

namespace ClinicalAgenda.Domain.Models
{
  public class User : Entity
  {
    public String Surname { get; set; }
    public String Name { get; set; }
    public String Email { get; set; }
    public String Password { get; set; }

  }
}