﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalAgenda.Domain.Models
{
  public class Schedule : Entity
  {
    public DateTime StartDate { get; set; }
    public TimeSpan StartTime { get; set; }
    public TimeSpan SessionDuration { get; set; }
    public Client Client { get; set; }
    public Treatment Treatment { get; set; }
  }
}
