﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClinicalAgenda.Domain.Models
{
  public class Client : User
  {

    public String Document { get; set; }
    public String Cellphone { get; set; }
    public String Address { get; set; }

  }
}
