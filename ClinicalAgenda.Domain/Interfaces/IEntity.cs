﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain
{
  public interface IEntity
  {
    int Id { get; set; }

  }
}
