﻿using System;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Domain.Interfaces.Services
{
  public interface ITreatmentService : IService<Treatment>
  {

  }
}
