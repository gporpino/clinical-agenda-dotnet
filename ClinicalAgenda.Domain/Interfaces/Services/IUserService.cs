﻿using System;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Domain.Interfaces.Services
{
  public interface IUserService : IService<User>
  {
    User Authenticate(String email, String password);

    IList<Client> GetAllClients();

    Client FindClient(int id);
  }
}
