﻿using System;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Domain.Interfaces.Services
{
  public interface IClientService : IService<Client>
  {

  }
}
