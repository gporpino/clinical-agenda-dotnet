﻿using System;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Domain.Interfaces.Services
{
  public interface IScheduleService : IService<Schedule>
  {
    List<Schedule> GetMySchedules(User user);
    List<Schedule> SelectByClient(Client client);
  }
}
