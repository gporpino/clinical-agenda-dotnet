﻿using System;
using System.Collections.Generic;
using ClinicalAgenda.Domain.Interfaces.Validations;

namespace ClinicalAgenda.Domain.Interfaces.Services
{
  public interface IService<T> where T : IEntity
  {
    IValidationResult<T> Add(T t);
    T Find(int id);
    List<T> GetAll();

    IValidationResult<T> Update(T t);
    IValidationResult<T> Remove(T t);
    IValidationResult<T> Remove(int id);
    IValidationResult<T> Validate(T t, ValidationType type);

  }
}