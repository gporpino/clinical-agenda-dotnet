using ClinicalAgenda.Domain.Interfaces.Services;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Domain.Interfaces
{
  public interface IServiceFacade
  {
    IScheduleService GetScheduleService();
    ITreatmentService GetTreatmentService();
    IUserService GetUserService();
  }
}