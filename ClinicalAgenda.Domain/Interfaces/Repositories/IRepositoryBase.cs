﻿
using System.Collections.Generic;
using ClinicalAgenda.Domain.Models;

namespace ClinicalAgenda.Domain.Interfaces.Repositories
{
  public interface IRepository<T> where T : IEntity
  {
    void Add(T t);
    T Find(int id);
    List<T> GetAll();
    void Update(T t);
    void Update(int Id, T t);
    void Delete(T t);
    void Delete(int id);
    int Size();

  }
}
