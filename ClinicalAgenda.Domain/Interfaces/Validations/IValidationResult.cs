using System;
using System.Collections.Generic;

namespace ClinicalAgenda.Domain.Interfaces.Validations
{
  public interface IValidationResult<T> where T : IEntity
  {
    IEntity Entity { get; }
    bool IsValid();
    void AddError(IValidation<T> validation, T t);

    IList<IValidationError<T>> Errors { get; }
  }
}