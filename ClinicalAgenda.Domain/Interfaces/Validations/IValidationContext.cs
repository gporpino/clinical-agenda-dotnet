using ClinicalAgenda.Domain.Interfaces.Services;

namespace ClinicalAgenda.Domain.Interfaces.Validations
{
  public interface IValidationContext<T> where T : IEntity
  {
    IService<T> Service { get; set; }
    T Entity { get; set; }
  }
}