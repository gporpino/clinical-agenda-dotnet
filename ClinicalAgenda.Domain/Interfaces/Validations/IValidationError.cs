namespace ClinicalAgenda.Domain.Interfaces.Validations
{
  public interface IValidationError<T> where T : IEntity
  {
    IValidation<T> Validation { get; }
    T Entity { get; }
    string GetMessage();
    string GetField();


  }
}