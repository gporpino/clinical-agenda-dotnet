namespace ClinicalAgenda.Domain.Interfaces.Validations
{
  public interface IValidation<T> where T : IEntity
  {
    IValidator<T> Validator { get; }
    ValidationType Type { get; }
  }
}