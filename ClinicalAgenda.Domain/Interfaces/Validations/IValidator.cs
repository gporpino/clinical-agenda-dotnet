

using System;

namespace ClinicalAgenda.Domain.Interfaces.Validations
{
  public interface IValidator<T> where T : IEntity
  {
    bool Validate(IValidationContext<T> context);
    String Message { get; }
    String Field { get; }
  }
}