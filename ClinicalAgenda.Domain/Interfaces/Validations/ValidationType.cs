namespace ClinicalAgenda.Domain.Interfaces.Validations
{
  public enum ValidationType { CREATE, UPDATE, DELETE }

}