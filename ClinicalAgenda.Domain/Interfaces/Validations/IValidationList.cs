using System.Collections.Generic;

namespace ClinicalAgenda.Domain.Interfaces.Validations
{

  public interface IValidationList<T> where T : IEntity
  {
    void Add(IValidator<T> validator, params ValidationType[] types);

    IList<IValidation<T>> GetValidations();
  }
}